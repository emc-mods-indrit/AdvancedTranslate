package me.Indrit.translate.event;

import me.Indrit.translate.command.CommandManager;
import me.Indrit.translate.event.events.EventFirstStart;
import me.Indrit.translate.event.events.EventPluginCoreLoadedSuccessful;

import me.Indrit.translate.gui.MainScreen;
import me.Indrit.translate.utils.ModLogger;
import me.deftware.client.framework.chat.LiteralChatMessage;
import me.deftware.client.framework.event.EventHandler;
import me.deftware.client.framework.event.EventListener;
import me.deftware.client.framework.event.events.EventGuiScreenDraw;
import me.deftware.client.framework.event.events.EventShutdown;
import me.deftware.client.framework.gui.minecraft.ScreenInstance;
import me.deftware.client.framework.gui.widgets.Button;
import me.deftware.client.framework.minecraft.Minecraft;

import static me.Indrit.translate.utils.message.ChatUtils.sendClientMessage;
import static me.Indrit.translate.utils.message.ChatUtils.sendDebugMessage;

public class CoreEventListener extends EventListener {

    @EventHandler
    private void onShutDown(EventShutdown event) {
        ModLogger.info("Shut down");
    }

    @EventHandler
    public void onCoreLoad(EventPluginCoreLoadedSuccessful event){
        ModLogger.info("Plugin core has been loaded");
        sendDebugMessage("Plugin Core has been loaded successfully");
        CommandManager.addCommands();
        ModLogger.info("Loaded Commands");
        sendDebugMessage("Loaded Commands");
    }

    @EventHandler
    public void onFirstStartUp(EventFirstStart event){
        sendClientMessage("This plugin is in early alpha stage");
        sendClientMessage("So expect bugs and at times crashes, if these occurs:");
        sendClientMessage("Please contact Indrit#7670 on discord with information about the bug/crash");
    }

    @EventHandler
    private void drawButton(EventGuiScreenDraw event) {
        if (event.getInstance().getType() == ScreenInstance.CommonScreenTypes.GuiIngameMenu) {
            event.addButton(new Button(0,  event.getWidth() / 2 - 100, event.getHeight() - 28 , 200, 20, new LiteralChatMessage("Advanced Translator")) {
                @Override
                public void onButtonClick(double mouseX, double mouseY) {
                    Minecraft.openScreen(new MainScreen(null));
                }
            });

        }
    }

}
