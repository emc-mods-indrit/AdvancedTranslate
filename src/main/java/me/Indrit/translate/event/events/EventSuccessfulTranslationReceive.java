package me.Indrit.translate.event.events;

import me.Indrit.translate.translation.TranslationResult;
import me.deftware.client.framework.event.Event;

public class EventSuccessfulTranslationReceive extends Event {

    private final TranslationResult translationResult;

    public EventSuccessfulTranslationReceive(TranslationResult tr) {
        this.translationResult = tr;
    }

    public TranslationResult getTranslationResult() {
        return translationResult;
    }
}
