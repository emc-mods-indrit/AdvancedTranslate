package me.Indrit.translate.event.events;

import me.Indrit.translate.settings.Settings;
import me.deftware.client.framework.event.Event;

public class EventFirstStart extends Event {

    public EventFirstStart() {
        //Not first start from now on...
        Settings.put(Settings.IS_FIRST_START, false);
    }
}
