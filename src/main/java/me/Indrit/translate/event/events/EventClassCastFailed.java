package me.Indrit.translate.event.events;

import me.Indrit.translate.utils.ModLogger;
import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.minecraft.Minecraft;

import static me.Indrit.translate.utils.message.ChatUtils.sendErrorMessage;

public class EventClassCastFailed extends Event {

    public EventClassCastFailed(String errorMessage, Throwable err) {
        if(Minecraft.getScreen() == null) {
            sendErrorMessage(errorMessage, err.getMessage());
        }
        ModLogger.error(err);
    }
}
