package me.Indrit.translate.event;

import me.Indrit.translate.Main;
import me.Indrit.translate.event.events.EventSuccessfulTranslationReceive;
import me.Indrit.translate.event.events.EventSuccessfulTranslationSend;
import me.Indrit.translate.settings.Settings;
import me.Indrit.translate.translation.TranslationResult;
import me.Indrit.translate.translation.online.TranslationOnlineResult;
import me.Indrit.translate.utils.message.ClickEvent;
import me.Indrit.translate.utils.message.CustomMessageBuilder;
import me.deftware.client.framework.chat.ChatMessage;
import me.deftware.client.framework.chat.builder.ChatBuilder;
import me.deftware.client.framework.event.EventHandler;
import me.deftware.client.framework.event.EventListener;
import me.deftware.client.framework.event.events.EventChatReceive;
import me.deftware.client.framework.event.events.EventChatSend;
import me.deftware.client.framework.main.bootstrap.Bootstrap;

import static me.Indrit.translate.settings.Settings.*;

public class TranslationEventListener extends EventListener {

    private String lastPlayerMessage;
    private TranslationResult lastPlayerTranslateResult;

    @EventHandler
    private void onChatSend(EventChatSend event) {
        if(!event.getMessage().startsWith("/") && !event.getMessage().startsWith(Bootstrap.EMCSettings.getPrimitive("commandtrigger","."))) {
            if(Settings.get(SHOULD_TRANSLATE_SEND)) {
                try {
                    TranslationResult tr = Main.translator.
                            setSourceLang(Settings.get(LANGUAGE_FROM_SEND)).
                            setTargetLang(Settings.get(LANGUAGE_TO_SEND)).
                            setTranslateQuery(event.getMessage()).
                            requestTranslation().
                            get();
                    event.setMessage(tr.getRequestOut());
                    new EventSuccessfulTranslationSend(tr).broadcast();
                } catch (Exception e) {
                    event.setCanceled(true);
                }
            } else {
                lastPlayerMessage = event.getMessage();
            }
        }
    }

    @EventHandler(priority = 10000)
    private void onChatReceive(EventChatReceive event) {
        //TODO: This function is a troll, will be changed later (Its alpha boizzzz, soo whoo cares XDDD)
        long time = System.currentTimeMillis();
        ChatMessage message = event.getMessage();
        if (lastPlayerTranslateResult != null && message.toString(false).contains(lastPlayerTranslateResult.getRequestOut())) {
            event.setMessage(CustomMessageBuilder.build(message, new ChatBuilder().withText(lastPlayerTranslateResult.getRequestIn()).build(), ClickEvent.NONE,true, time));
        } else if (lastPlayerMessage != null && message.toString(false).contains(lastPlayerMessage)){
            event.setMessage(CustomMessageBuilder.build(message, null, ClickEvent.NONE,false, time));
        } else if (Settings.get(SHOULD_TRANSLATE_RECEIVE)) {
            //TODO: PEE PEE POO POO
        } else {
            if(!message.toString(false).equals("")){
                if (Settings.get(SHOULD_TRANSLATE_ON_CLICK)) {
                    event.setMessage(CustomMessageBuilder.build(message, null, ClickEvent.TRANSLATE,false, time));
                }
            }else{
                event.setMessage(CustomMessageBuilder.build(message, null, ClickEvent.NONE,false, time));
            }
        }
    }

    @EventHandler
    private void onSuccessfulTranslation(EventSuccessfulTranslationReceive event) {
        long time = System.currentTimeMillis();
        if (event.getTranslationResult() instanceof TranslationOnlineResult) {
            CustomMessageBuilder.build(new ChatBuilder().withText(event.getTranslationResult().getRequestOut()).build(), new ChatBuilder().withText(event.getTranslationResult().getRequestIn()).build(), ClickEvent.NONE,true, time);
        }
    }

    @EventHandler
    private void onSuccessfulTranslation(EventSuccessfulTranslationSend event) {
        lastPlayerMessage = event.getTranslationResult().getRequestOut();
        lastPlayerTranslateResult = event.getTranslationResult();
    }
}