package me.Indrit.translate.utils.message;

import me.Indrit.translate.settings.Settings;
import me.deftware.client.framework.chat.ChatMessage;
import me.deftware.client.framework.chat.builder.ChatBuilder;
import me.deftware.client.framework.chat.style.ChatColors;

import static me.Indrit.translate.settings.Settings.DEBUG_TO_CHAT;
import static me.Indrit.translate.translation.exceptions.ErrorMessageHelper.buildErrorChatMessage;

public class ChatUtils {

    public static ChatMessage getPrefix(String prefix) {
        return new ChatBuilder().withText(prefix).withColor(ChatColors.DARK_PURPLE).setBold()
                .append().withSpace().withChar(ChatBuilder.getChevron()).withColor(ChatColors.GRAY).append().withSpace().build();
    }

    public static void sendErrorMessage(String message, String hover) {
        sendErrorMessage(buildErrorChatMessage(message, hover));
    }

    public static void sendErrorMessage(ChatMessage message) {
        sendClientMessage(message);
    }

    public static void sendClientMessage(ChatMessage message) {
       getPrefix("AT").join(message).print();
    }

    public static void sendClientMessage(String message) {
        sendClientMessage(new ChatBuilder().withText(message).withColor(ChatColors.GRAY).build());
    }

    public static void sendPublicMessage(String message) {
        sendPublicMessage(new ChatBuilder().withText(message).build());
    }

    public static void sendPublicMessage(ChatMessage message) {
        new ChatBuilder().withMessage(message).build().sendMessage();
    }

    public static void sendDebugMessage(ChatMessage message) {
        if(Settings.get(DEBUG_TO_CHAT)) {
            sendClientMessage(message);
        }
    }

    public static void sendDebugMessage(String message) {
        sendDebugMessage(new ChatBuilder().withText(message).withColor(ChatColors.YELLOW).build());
    }
}
