package me.Indrit.translate.utils.message;

import me.deftware.client.framework.chat.ChatMessage;
import me.deftware.client.framework.chat.builder.ChatBuilder;
import me.deftware.client.framework.chat.event.ChatClickEvent;

import java.time.*;
import java.time.format.DateTimeFormatter;

import static me.Indrit.translate.utils.Utils.getLocalZone;

public class CustomMessageBuilder {
    private final static String NEWLINE = "\n";
    final static DateTimeFormatter formatter =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static ChatMessage build(ChatMessage message, ChatMessage original, ClickEvent event, boolean translated, long time) {
        ChatBuilder hoverMessage = new ChatBuilder();
        ChatBuilder prefixMessage = new ChatBuilder();

        String formattedDtm = Instant.ofEpochMilli(time)
                .atZone(getLocalZone())
                .format(formatter);


        hoverMessage.withText("Time: " + formattedDtm + NEWLINE).append();
        hoverMessage.withText("Event: " + event.name() + NEWLINE).append();
        hoverMessage.withText("Translated: " + translated).append();
        if(original != null){
            hoverMessage.withText(NEWLINE + "Original: ").append();
            hoverMessage.withMessage(original).append();
        }

        prefixMessage.
                withText("\u00A75\u00A7lAT \u00A77" + ChatBuilder.getChevron() + " ").
                withHover(hoverMessage.build());

        if(event == ClickEvent.TRANSLATE){
            prefixMessage.withClickEvent(new ChatClickEvent(ChatClickEvent.EventType.RUN_COMMAND, ".at translate " + message.toString(false)));
        }

        return prefixMessage.build().join(message);
    }
}
