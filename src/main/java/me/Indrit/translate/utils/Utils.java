package me.Indrit.translate.utils;

import me.Indrit.translate.translation.exceptions.NoInternetConnectionException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.time.*;
import java.util.Scanner;

public class Utils {

    public static String HttpRequest(String url) {
        try {
            URL Url = new URL(url);
            StringBuilder response = new StringBuilder();
            HttpURLConnection con = (HttpURLConnection) Url.openConnection();
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return null;
    }

    public static void writeFile(String canonicalFilename, String text) {
        File file = new File(canonicalFilename);
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(file));
            out.write(text);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String openFile(String dir) {
        try {
            File file = new File(dir);
            Scanner fileReader = new Scanner(file);
            while (fileReader.hasNextLine()) {
                String data = fileReader.nextLine();
            }
            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isFile(String dir) {
        return new File(dir).isFile();
    }

    public static boolean checkNetConnection() {
        try (Socket socket = new Socket()) {
            int port = 80;
            InetSocketAddress socketAddress = new InetSocketAddress("google.com", port);
            socket.connect(socketAddress, 200);
            return true;
        } catch (IOException e) {
            throw new NoInternetConnectionException();
        }
    }

    public static ZoneId getLocalZone(){
        return ZoneId.systemDefault();
    }
}
