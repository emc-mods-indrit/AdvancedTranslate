package me.Indrit.translate.utils;

import me.Indrit.translate.settings.Settings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ModLogger {
    private static Logger logger = LogManager.getLogger("Advanced Translate");

    public static <T> void error(T input){
        if(Settings.get(Settings.LOG_TO_CONSOLE)){
            logger.error(input);
        }
    }

    public static <T> void info(T input){
        if(Settings.get(Settings.LOG_TO_CONSOLE)){
            logger.info(input);
        }
    }

    public static <T> void debug(T input){
        if(Settings.get(Settings.LOG_TO_CONSOLE)){
            logger.debug(input);
        }
    }
}
