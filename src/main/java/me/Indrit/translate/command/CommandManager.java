package me.Indrit.translate.command;
import me.Indrit.translate.command.commands.CommandLanguage;
import me.deftware.client.framework.command.CommandRegister;
import me.deftware.client.framework.command.EMCModCommand;

import java.util.ArrayList;

public class CommandManager {

    private static ArrayList<EMCModCommand> commands = new ArrayList<>();

    public static void addCommands() {
        commands.add(new CommandLanguage());

        commands.forEach(CommandRegister::registerCommand);
    }

    public static void register(EMCModCommand command) {
        commands.add(command);
        CommandRegister.registerCommand(command);
    }
}
