package me.Indrit.translate.command.commands;

import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import me.Indrit.translate.Main;
import me.Indrit.translate.settings.Settings;
import me.Indrit.translate.translation.TranslationLanguages;
import me.Indrit.translate.translation.exceptions.InvalidLanguageException;
import me.Indrit.translate.utils.message.ClickEvent;
import me.Indrit.translate.utils.message.CustomMessageBuilder;
import me.deftware.client.framework.chat.builder.ChatBuilder;
import me.deftware.client.framework.command.CommandBuilder;
import me.deftware.client.framework.command.CommandResult;
import me.deftware.client.framework.command.EMCModCommand;

import static me.Indrit.translate.settings.Settings.*;
import static me.Indrit.translate.utils.message.ChatUtils.sendClientMessage;

public class CommandLanguage extends EMCModCommand {
    @Override
    public CommandBuilder<?> getCommandBuilder() {
        return new CommandBuilder<>().set(LiteralArgumentBuilder.literal("advanced-translate")
                .then(
                        LiteralArgumentBuilder.literal("language")
                                .then(
                                        LiteralArgumentBuilder.literal("send")
                                                .then(
                                                        LiteralArgumentBuilder.literal("from")
                                                                .then(
                                                                        RequiredArgumentBuilder.argument("lang_from", StringArgumentType.string())
                                                                                .executes(c -> {
                                                                                    CommandResult r = new CommandResult(c);
                                                                                    try {
                                                                                        String language;
                                                                                        if (!r.getString("lang_from").equals("auto")) {
                                                                                            language = TranslationLanguages.getCodeFromUnkownInputType(r.getString("lang_from"));
                                                                                        } else {
                                                                                            language = r.getString("lang_from");
                                                                                        }
                                                                                        Settings.put(LANGUAGE_FROM_SEND, language);
                                                                                        sendClientMessage("Changed original language to " + language);
                                                                                    } catch(InvalidLanguageException ex){
                                                                                    }

                                                                                    return 1;
                                                                                })
                                                                )
                                                )
                                                .then(
                                                        LiteralArgumentBuilder.literal("to")
                                                                .then(
                                                                        RequiredArgumentBuilder.argument("lang_to", StringArgumentType.string())
                                                                                .executes(c -> {
                                                                                    CommandResult r = new CommandResult(c);
                                                                                    try {
                                                                                        String language = TranslationLanguages.getCodeFromUnkownInputType(r.getString("lang_to"));
                                                                                        Settings.put(LANGUAGE_TO_SEND, language);
                                                                                        sendClientMessage("Changed target language to " + language);
                                                                                    } catch(InvalidLanguageException ex){
                                                                                    }
                                                                                    return 1;
                                                                                })
                                                                )
                                                )
                                )
                                .then(
                                        LiteralArgumentBuilder.literal("receive")
                                                .then(
                                                        LiteralArgumentBuilder.literal("from")
                                                                .then(
                                                                        RequiredArgumentBuilder.argument("lang_from", StringArgumentType.string())
                                                                                .executes(c -> {
                                                                                    CommandResult r = new CommandResult(c);
                                                                                    try {
                                                                                        String language;
                                                                                        if (!r.getString("lang_from").equals("auto")) {
                                                                                            language = TranslationLanguages.getCodeFromUnkownInputType(r.getString("lang_from"));
                                                                                        } else {
                                                                                            language = r.getString("lang_from");
                                                                                        }
                                                                                        Settings.put(LANGUAGE_FROM_RECEIVE, language);
                                                                                        sendClientMessage("Changed original language to " + language);
                                                                                    } catch(InvalidLanguageException ex){
                                                                                    }

                                                                                    return 1;
                                                                                })
                                                                )
                                                )
                                                .then(
                                                        LiteralArgumentBuilder.literal("to")
                                                                .then(
                                                                        RequiredArgumentBuilder.argument("lang_to", StringArgumentType.string())
                                                                                .executes(c -> {
                                                                                    CommandResult r = new CommandResult(c);
                                                                                    try {
                                                                                        String language = TranslationLanguages.getCodeFromUnkownInputType(r.getString("lang_to"));
                                                                                        Settings.put(LANGUAGE_TO_RECEIVE, language);
                                                                                        sendClientMessage("Changed target language to " + language);
                                                                                    } catch(InvalidLanguageException ex){
                                                                                    }
                                                                                    return 1;
                                                                                })
                                                                )
                                                )
                                )
                )
                .then(
                        LiteralArgumentBuilder.literal("toggle")
                                .then(
                                        LiteralArgumentBuilder.literal("ON")
                                                .executes(c -> {
                                                    Settings.put(Settings.SHOULD_TRANSLATE_SEND, true);
                                                    sendClientMessage("Turned auto translation on");
                                                    return 1;
                                                })
                                )
                                .then(
                                        LiteralArgumentBuilder.literal("OFF")
                                                .executes(c -> {
                                                    Settings.put(Settings.SHOULD_TRANSLATE_SEND, false);
                                                    sendClientMessage("Turned auto translation off");
                                                    return 1;
                                                })
                                )
                )
                .then(
                        LiteralArgumentBuilder.literal("debug")
                                .then(
                                        LiteralArgumentBuilder.literal("ON")
                                                .executes(c -> {
                                                    Settings.put(Settings.DEBUG_TO_CHAT, true);
                                                    sendClientMessage("Turned debug mode on");
                                                    return 1;
                                                })
                                )
                                .then(
                                        LiteralArgumentBuilder.literal("OFF")
                                                .executes(c -> {
                                                    Settings.put(Settings.DEBUG_TO_CHAT, false);
                                                    sendClientMessage("Turned debug mode off");
                                                    return 1;
                                                })
                                )
                )//TODO:  Make this custom command event (dont use run command)
                .then(
                        LiteralArgumentBuilder.literal("translate")
                                .then(
                                        RequiredArgumentBuilder.argument("message", StringArgumentType.greedyString())
                                                .executes(c -> {
                                                    try {
                                                        CustomMessageBuilder.build(
                                                                new ChatBuilder().
                                                                        withText(
                                                                                Main.translator.
                                                                                        setSourceLang(Settings.get(LANGUAGE_FROM_RECEIVE)).
                                                                                        setTargetLang(Settings.get(LANGUAGE_TO_RECEIVE)).
                                                                                        setTranslateQuery(new CommandResult(c).getString("message")).
                                                                                        requestTranslation().get().
                                                                                        getRequestOut()).
                                                                        build(),
                                                                new ChatBuilder().withText(new CommandResult(c).getString("message")).build(),
                                                                ClickEvent.NONE,
                                                                true,
                                                                System.currentTimeMillis()).print();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    return 1;
                                                })
                                )
                )
        ).registerAlias("at");
    }
}
