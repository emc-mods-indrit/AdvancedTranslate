package me.Indrit.translate.translation.online;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import me.Indrit.translate.translation.TranslationResult;

import java.nio.charset.StandardCharsets;

public class TranslationOnlineResult implements TranslationResult {

    private String detectedLang;
    private String detectedLangAccuracy;
    private String requestIn = "";
    private String requestOut = "";
    private String modelHash;
    private String modelName;
    private JsonArray raw;
    private float translationTime;


    public TranslationOnlineResult(String data) {
        this.raw = new JsonParser().parse(data).getAsJsonArray();
        JsonArray translatedArray = this.raw.get(0).getAsJsonArray();
        for(int i = 0; i < translatedArray.size(); i++){
            requestIn += translatedArray.get(i).getAsJsonArray().get(1).getAsString();
            requestOut += translatedArray.get(i).getAsJsonArray().get(0).getAsString();

        }
        //Minecraft doesnt support special character outside UTF-8 (Will kick you from servers)
        this.requestOut = new String((this.requestOut).getBytes(), StandardCharsets.UTF_8);
    }

    @Override
    public String getDetectedLang() {
        return null;
    }

    @Override
    public String getDetectedLangAccuracy() {
        return null;
    }

    @Override
    public String getRequestIn() {
        return this.requestIn;
    }

    @Override
    public String getRequestOut() {
        return this.requestOut;
    }

    @Override
    public String getModelHash() {
        return null;
    }

    @Override
    public String getModelName() {
        return null;
    }

    @Override
    public String getLocalMap() {
        return null;
    }

    @Override
    public float getTranslationTime() {
        return -1;
    }

    @Override
    public String toString() {
        return "TranslationOnlineResult{" + "source_lan:" + this.detectedLang + " source_acc:" + this.detectedLangAccuracy + " req_in:" + this.requestIn + " req_out:" + this.requestOut + " mod_hash:" + this.modelHash + " mod_name:" + this.modelName + " time:" + this.translationTime + " raw:" + this.raw + "}";
    }
}
