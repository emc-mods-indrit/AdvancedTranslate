package me.Indrit.translate.translation;

import me.Indrit.translate.Main;
import me.Indrit.translate.translation.online.TranslationOnlineResult;
import me.Indrit.translate.utils.Utils;

import java.net.URLEncoder;

public class TranslationInstance {

    private final String from;
    private final String to;
    private final String query;

    TranslationInstance(String from, String to, String query) {
        this.from = from;
        this.to = to;
        this.query = query;
    }

    private TranslationOnlineResult onlineTranslation() throws Exception {
        return new TranslationOnlineResult(Utils.HttpRequest(Main.transUrl.
                replace("SOURCE_LANG", this.from).
                replace("TARGET_LANG", this.to).
                replace("QUERY", URLEncoder.encode(this.query, "UTF-8")))
        );
    }

    public TranslationResult translate() throws Exception {
        return onlineTranslation();
    }

}
