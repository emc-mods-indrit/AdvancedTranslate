package me.Indrit.translate.translation;

import me.Indrit.translate.translation.exceptions.TranslationFailedException;

import java.util.concurrent.CompletableFuture;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

public class TranslationHandler {

    private String target_lang;
    private String source_lang;
    private String translation_query;

    public TranslationHandler setTargetLang(String targetLang) {
        this.target_lang = TranslationLanguages.getCodeFromUnkownInputType(targetLang);
        return this;
    }

    public TranslationHandler setSourceLang(String sourceLang) {
        if (sourceLang.equals("auto")) {
            this.source_lang = sourceLang;
            return this;
        }
        this.source_lang = TranslationLanguages.getCodeFromUnkownInputType(sourceLang);
        return this;
    }

    public TranslationHandler setTranslateQuery(String translationQuery) {
        this.translation_query = Pattern.compile("\u00a7[0-9a-fk-or]", CASE_INSENSITIVE).matcher(translationQuery).replaceAll("");
        return this;
    }

    public CompletableFuture<TranslationResult> requestTranslation() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return new TranslationInstance(this.source_lang, this.target_lang, this.translation_query).translate();
            } catch (Exception e) {
                throw new TranslationFailedException("Failed to translate (Hover to see more info)", e);
            }
        });
    }
}