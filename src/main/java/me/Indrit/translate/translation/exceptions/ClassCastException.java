package me.Indrit.translate.translation.exceptions;

import me.Indrit.translate.event.events.EventClassCastFailed;

public class ClassCastException extends RuntimeException {

    public ClassCastException(String errorMessage, Throwable err) {
        super(errorMessage, err);
        new EventClassCastFailed(errorMessage, err).broadcast();
    }
}
