package me.Indrit.translate.translation.exceptions;

import me.deftware.client.framework.chat.ChatMessage;
import me.deftware.client.framework.chat.builder.ChatBuilder;
import me.deftware.client.framework.chat.style.ChatColors;

public class ErrorMessageHelper {

    public static String NOT_SUPPORTED_LANGUAGE = "\'%input%\' is not a supported language";
    public static String NO_INTERNET_CONNECTION = "No internet connection could be established! Plugin wont load";

    public static String buildErrorMessageString(String input, String type) {
        return type.replace("%input%", input);
    }

    public static ChatMessage buildErrorChatMessage(String message, String hover) {
        ChatBuilder errorChatMessage = new ChatBuilder();
        errorChatMessage.withText(message).withColor(ChatColors.RED);
        if(hover != null){
            errorChatMessage.withHover(new ChatBuilder().withText(hover).withColor(ChatColors.RED).build());
        }
        return errorChatMessage.build();
    }

}
