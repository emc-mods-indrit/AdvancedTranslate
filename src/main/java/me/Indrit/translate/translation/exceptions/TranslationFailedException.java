package me.Indrit.translate.translation.exceptions;

import me.Indrit.translate.event.events.EventFailedTranslation;

public class TranslationFailedException extends RuntimeException {

    public TranslationFailedException(String errorMessage, Throwable err) {
        super(errorMessage, err);
        new EventFailedTranslation(errorMessage, err).broadcast();
    }
}
