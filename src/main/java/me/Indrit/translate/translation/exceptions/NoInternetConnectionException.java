package me.Indrit.translate.translation.exceptions;

import me.Indrit.translate.event.events.EventNoInternetConnection;

import static me.Indrit.translate.translation.exceptions.ErrorMessageHelper.NO_INTERNET_CONNECTION;

public class NoInternetConnectionException extends RuntimeException{

    public NoInternetConnectionException() {
        super(NO_INTERNET_CONNECTION);
        new EventNoInternetConnection(NO_INTERNET_CONNECTION).broadcast();
    }
}
