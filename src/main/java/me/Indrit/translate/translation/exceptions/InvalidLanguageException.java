package me.Indrit.translate.translation.exceptions;

import me.Indrit.translate.event.events.EventNotSupportedLanguage;

import static me.Indrit.translate.translation.exceptions.ErrorMessageHelper.NOT_SUPPORTED_LANGUAGE;
import static me.Indrit.translate.translation.exceptions.ErrorMessageHelper.buildErrorMessageString;

public class InvalidLanguageException extends RuntimeException {

    public InvalidLanguageException(String errorMessage) {
        super(buildErrorMessageString(errorMessage, NOT_SUPPORTED_LANGUAGE));
        new EventNotSupportedLanguage(buildErrorMessageString(errorMessage, NOT_SUPPORTED_LANGUAGE)).broadcast();
    }
}
