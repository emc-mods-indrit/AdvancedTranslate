package me.Indrit.translate.translation;

public interface TranslationResult {

    String getDetectedLang();

    String getDetectedLangAccuracy();

    String getRequestIn();

    String getRequestOut();

    String getModelHash();

    String getModelName();

    String getLocalMap();

    float getTranslationTime();
}