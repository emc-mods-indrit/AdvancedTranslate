package me.Indrit.translate.translation;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import me.Indrit.translate.Main;
import me.Indrit.translate.translation.exceptions.InvalidLanguageException;
import me.Indrit.translate.utils.Utils;

import java.util.Objects;

public class TranslationLanguages {

    private static BiMap<String, String> Languages;

    public static void cacheLanguages() {
        Languages = json2map(new JsonParser().parse(Objects.requireNonNull(Utils.HttpRequest(Main.langListUrl))).getAsJsonArray());
    }

    public static String getCodeFromUnkownInputType(String input) {
        String c4l = getCodeForLanguage(input);
        String l4c = getLanguageForCode(input);

        if (c4l != null) {
            return c4l;
        } else if (l4c != null) {
            return input;
        } else {
            throw new InvalidLanguageException(input);
        }
    }

    public static String getLanguageForCode(String key) {
        return Languages.get(key.toLowerCase());
    }

    public static String getCodeForLanguage(String key) {
        return Languages.inverse().get(key.toLowerCase());
    }

    public static BiMap<String, String> json2map(JsonArray languages) {
        BiMap<String, String> langMap = HashBiMap.create();
        for (JsonElement object : languages) {
            langMap.put(object.getAsJsonObject().get("code").getAsString().toLowerCase(), object.getAsJsonObject().get("name").getAsString().toLowerCase());
        }
        return langMap;
    }
}
