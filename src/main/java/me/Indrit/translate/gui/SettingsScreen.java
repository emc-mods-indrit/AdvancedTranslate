package me.Indrit.translate.gui;

import me.deftware.client.framework.chat.LiteralChatMessage;
import me.deftware.client.framework.gui.GuiScreen;
import me.deftware.client.framework.gui.minecraft.GuiSlot;
import me.deftware.client.framework.gui.widgets.Button;
import me.deftware.client.framework.minecraft.Minecraft;
import org.lwjgl.glfw.GLFW;

public class SettingsScreen extends GuiScreen {
    private SettingList settingList;
    public SettingsScreen(GuiScreen parent) {
        super(parent);
    }

    @Override
    protected void onInitGui() {
        this.clearButtons();
        this.clearTexts();
        settingList = new SettingList(this);
        this.addButton(new Button(0, getGuiScreenWidth() / 2 - 100, getGuiScreenHeight() - 28, 200, 20, new LiteralChatMessage("Back")) {
            @Override
            public void onButtonClick(double v, double v1) {
                Minecraft.openScreen(parent);
            }
        });
        addCenteredText(getGuiScreenWidth() / 2, 8, new LiteralChatMessage("Settings"));
    }

    @Override
    protected void onDraw(int i, int i1, float v) {
        this.renderBackgroundTextureWrap(0);
        settingList.doDraw(i,i1,v);
    }

    @Override
    protected void onUpdate() {

    }

    @Override
    protected void onKeyPressed(int i, int i1, int i2) {
        if (i == GLFW.GLFW_KEY_ESCAPE) {
            Minecraft.openScreen(parent);
        }
    }

    @Override
    protected void onMouseReleased(int i, int i1, int i2) {

    }

    @Override
    protected void onMouseClicked(int i, int i1, int i2) {

    }

    @Override
    protected void onGuiResize(int i, int i1) {

    }

    private static class SettingList extends GuiSlot {
        SettingList(GuiScreen parent) {
            super(parent.getGuiScreenWidth(), parent.getGuiScreenHeight(), 36, parent.getGuiScreenHeight() - 56, 54);
        }

        @Override
        protected int getISize() {
            return 0;
        }

        @Override
        protected void drawISlot(int i, int i1, int i2) {
        }
    }
}