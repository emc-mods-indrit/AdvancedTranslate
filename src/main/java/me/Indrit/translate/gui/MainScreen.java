package me.Indrit.translate.gui;

import me.deftware.client.framework.chat.LiteralChatMessage;
import me.deftware.client.framework.gui.GuiScreen;
import me.deftware.client.framework.gui.widgets.Button;
import me.deftware.client.framework.minecraft.Minecraft;
import org.lwjgl.glfw.GLFW;

public class MainScreen extends GuiScreen {
    public MainScreen(GuiScreen parent) {
        super(parent);
    }

    @Override
    protected void onInitGui() {
        int start_y = getGuiScreenHeight() - getGuiScreenHeight()/10;
        int button_height = 20;
        int button_spacing = 28;

        this.clearButtons();
        this.clearTexts();
        this.addButton(new Button(0, getGuiScreenWidth() / 2 - 100, start_y, 200, button_height, new LiteralChatMessage("Back")) {
            @Override
            public void onButtonClick(double v, double v1) {
                Minecraft.openScreen(parent);
            }
        });
        this.addButton(new Button(0, getGuiScreenWidth() / 2 - 100, start_y - button_height - button_spacing, 200, button_height, new LiteralChatMessage("Settings")) {
            @Override
            public void onButtonClick(double v, double v1) {
                Minecraft.openScreen(new SettingsScreen(MainScreen.this));
            }
        });
        this.addButton(new Button(0, getGuiScreenWidth() / 2 - 100, start_y - 2*button_height - 2*button_spacing, 200, button_height, new LiteralChatMessage("Quick Translate")) {
            @Override
            public void onButtonClick(double v, double v1) {
                Minecraft.openScreen(new TranslateWindowScreen(MainScreen.this));
            }
        });
        addCenteredText(getGuiScreenWidth() / 2, 8, new LiteralChatMessage("Advanced Translation"));
    }

    @Override
    protected void onDraw(int mouseX, int mouseY, float partialTicks) {
        this.renderBackgroundTextureWrap(0);
    }

    @Override
    protected void onKeyPressed(int keyCode, int action, int modifiers) {
        if (keyCode == GLFW.GLFW_KEY_ESCAPE) {
            Minecraft.openScreen(parent);
        }
    }

    @Override
    protected void onUpdate() {
    }

    @Override
    protected void onMouseReleased(int mouseX, int mouseY, int mouseButton) { }

    @Override
    protected void onMouseClicked(int mouseX, int mouseY, int mouseButton) { }

    @Override
    protected void onGuiResize(int w, int h) { }

}
