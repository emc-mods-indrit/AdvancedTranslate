package me.Indrit.translate.gui;

import me.Indrit.translate.Main;
import me.Indrit.translate.settings.Settings;
import me.Indrit.translate.translation.TranslationResult;
import me.deftware.client.framework.chat.LiteralChatMessage;
import me.deftware.client.framework.gui.GuiScreen;
import me.deftware.client.framework.gui.widgets.Button;
import me.deftware.client.framework.gui.widgets.TextField;
import me.deftware.client.framework.minecraft.Minecraft;
import org.lwjgl.glfw.GLFW;

public class TranslateWindowScreen extends GuiScreen {
    private TextField sourceLanguage;
    private TextField targetLanguage;
    private TextField input;
    private TextField output;

    public TranslateWindowScreen(GuiScreen parent) {
        super(parent);
    }

    @Override
    protected void onInitGui() {
        this.clearButtons();
        this.clearTexts();
        this.addButton(new Button(0, getGuiScreenWidth() / 2 - 100, getGuiScreenHeight() - 28, 200, 20, new LiteralChatMessage("Back")) {
            @Override
            public void onButtonClick(double v, double v1) {
                Minecraft.openScreen(parent);
            }
        });

        addCenteredText(getGuiScreenWidth() / 2, 8, new LiteralChatMessage("Quick Translation"));

        addText(getGuiScreenWidth() / 2 - 50, getGuiScreenHeight() / 2, new LiteralChatMessage("From"));
        sourceLanguage = new TextField(0, getGuiScreenWidth() / 2, getGuiScreenHeight() / 2 - 40, 100, 20);
        addText(getGuiScreenWidth() / 2 - 50, getGuiScreenHeight() / 2, new LiteralChatMessage("To"));
        targetLanguage = new TextField(1, getGuiScreenWidth() / 2, getGuiScreenHeight() / 2 - 40, 100, 20);

        addText(getGuiScreenWidth() / 2 - 50, getGuiScreenHeight() / 2, new LiteralChatMessage("To"));
        input = new TextField(2, getGuiScreenWidth() / 2 - 150, getGuiScreenHeight() - getGuiScreenHeight()/3 - 30, 300, 20);
        addText(getGuiScreenWidth() / 2 - 50, getGuiScreenHeight() / 2, new LiteralChatMessage("To"));
        output = new TextField(3, getGuiScreenWidth() / 2 - 150, getGuiScreenHeight() - getGuiScreenHeight()/3, 300, 20);

        sourceLanguage.setTextboxText(Settings.get(Settings.LANGUAGE_FROM_RECEIVE));
        targetLanguage.setTextboxText(Settings.get(Settings.LANGUAGE_TO_RECEIVE));

        this.addEventListener(sourceLanguage);
        this.addEventListener(targetLanguage);
        this.addEventListener(input);

        this.addButton(new Button(0, getGuiScreenWidth() / 2 - 150, getGuiScreenHeight() - 100, 100, 20, new LiteralChatMessage("Translate")) {
            @Override
            public void onButtonClick(double v, double v1) {
                try {
                    TranslationResult tr = Main.translator.
                            setSourceLang(sourceLanguage.getTextboxText()).
                            setTargetLang(targetLanguage.getTextboxText()).
                            setTranslateQuery(input.getTextboxText()).
                            requestTranslation().
                            get();
                    output.setTextboxText(tr.getRequestOut());
                }catch(Exception ex){
                    addCenteredText(getGuiScreenWidth() / 2, getGuiScreenHeight() - 60, new LiteralChatMessage(ex.toString()));
                }
            }
        });

    }

    @Override
    protected void onDraw(int i, int i1, float v) {
        this.renderBackgroundTextureWrap(0);
        sourceLanguage.onDraw(i, i1, v);
        targetLanguage.onDraw(i, i1, v);
        input.onDraw(i, i1, v);
        output.onDraw(i, i1, v);
    }

    @Override
    protected void onUpdate() {

    }

    @Override
    protected void onKeyPressed(int i, int i1, int i2) {
        if (i == GLFW.GLFW_KEY_ESCAPE) {
            Minecraft.openScreen(parent);
        }
    }

    @Override
    protected void onMouseReleased(int i, int i1, int i2) {

    }

    @Override
    protected void onMouseClicked(int i, int i1, int i2) {

    }

    @Override
    protected void onGuiResize(int i, int i1) {

    }
}
