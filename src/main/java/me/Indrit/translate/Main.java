package me.Indrit.translate;

import me.Indrit.translate.event.CoreEventListener;
import me.Indrit.translate.event.TranslationEventListener;
import me.Indrit.translate.event.events.EventFirstStart;
import me.Indrit.translate.event.events.EventPluginCoreLoadedSuccessful;
import me.Indrit.translate.gui.MainScreen;
import me.Indrit.translate.settings.Settings;
import me.Indrit.translate.translation.TranslationHandler;
import me.Indrit.translate.translation.TranslationLanguages;
import me.Indrit.translate.utils.Utils;
import me.deftware.client.framework.gui.GuiScreen;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.minecraft.Minecraft;

import java.io.File;

public class Main extends EMCMod {
    public static final String langListUrl = "https://gitlab.com/emc-mods-indrit/AdvancedTranslate/raw/master/src/main/resources/languages.json";
    public static final String langCacheUrl = "https://gitlab.com/emc-mods-indrit/AdvancedTranslate/raw/master/src/main/resources/language_cache_base.json";
    public static final String transUrl = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=SOURCE_LANG&tl=TARGET_LANG&dt=t&q=QUERY";
    public static TranslationHandler translator;
    public static File modFile;
    private static Main instance;

    public static Main getInstance() {
        return instance;
    }

    @Override
    public void initialize() {
        instance = this;
        modFile = instance.physicalFile;
        new CoreEventListener();
        new EventPluginCoreLoadedSuccessful().broadcast();
        if (Utils.checkNetConnection()) {
            new TranslationEventListener();
            translator = new TranslationHandler();
            TranslationLanguages.cacheLanguages();
            if (Settings.get(Settings.IS_FIRST_START)) {
                new EventFirstStart().broadcast();
            }
        }
    }

    @Override
    public void callMethod(String method, String caller, Object parent) {
        if (method.equals("openGUI()")) {
            Minecraft.openScreen(new MainScreen((GuiScreen) parent));
        }
    }

}


