package me.Indrit.translate.settings;

import me.Indrit.translate.Main;
import me.Indrit.translate.event.events.EventClassCastFailed;
import me.Indrit.translate.translation.exceptions.ClassCastException;

public class Settings {

    public static me.deftware.client.framework.config.Settings config = Main.getInstance().getSettings();

    public static <T> void put(SettingType setting, T val){

        try {
            //TODO: Find a way to not be sent to hell for this sin
            switch (val.getClass().getSimpleName()){
                case "Integer":
                    config.putPrimitive(setting.getKey(), (Integer) val);
                    break;
                case "Float":
                    config.putPrimitive(setting.getKey(), (Float) val);
                    break;
                case "Double":
                    config.putPrimitive(setting.getKey(), (Double) val);
                    break;
                case "String":
                    config.putPrimitive(setting.getKey(), (String) val);
                    break;
                case "Character":
                    config.putPrimitive(setting.getKey(), (Character) val);
                    break;
                case "Long":
                    config.putPrimitive(setting.getKey(), (Long) val);
                    break;
                case "Byte":
                    config.putPrimitive(setting.getKey(), (Byte) val);
                    break;
                case "Short":
                    config.putPrimitive(setting.getKey(), (Short) val);
                    break;
                case "Boolean":
                    config.putPrimitive(setting.getKey(), (Boolean) val);
                    break;
            }
        } catch(java.lang.ClassCastException e) {
            new EventClassCastFailed("Something fucked up real hard here!", e).broadcast();
        }
    }

    public static <T> T get(SettingType setting){
        try {
            //TODO: Find a way to not be sent to hell for this sin
            switch (setting.getDefVal().getClass().getSimpleName()) {
                case "Integer":
                    return (T) new Integer(config.getPrimitive(setting.getKey(), (Integer) setting.getDefVal()));
                case "Float":
                    return (T) new Float(config.getPrimitive(setting.getKey(), (Float) setting.getDefVal()));
                case "Double":
                    return (T) new Double(config.getPrimitive(setting.getKey(), (Double) setting.getDefVal()));
                case "String":
                    return (T) new String(config.getPrimitive(setting.getKey(), (String) setting.getDefVal()));
                case "Character":
                    return (T) new Character(config.getPrimitive(setting.getKey(), (Character) setting.getDefVal()));
                case "Long":
                    return (T) new Long(config.getPrimitive(setting.getKey(), (Long) setting.getDefVal()));
                case "Byte":
                    return (T) new Byte(config.getPrimitive(setting.getKey(), (Byte) setting.getDefVal()));
                case "Short":
                    return (T) new Short(config.getPrimitive(setting.getKey(), (Short) setting.getDefVal()));
                case "Boolean":
                    return (T) new Boolean(config.getPrimitive(setting.getKey(), (Boolean) setting.getDefVal()));
            }
        } catch(Exception e) {
            throw new ClassCastException("Something fucked up real hard here!", e);
        }
        return null;
    }

    // All the settings with the keys and values taken care of for us from here

    /* --------------------------Core------------------------ */
    public static SettingType<Boolean> IS_FIRST_START = new SettingType<Boolean>("is_first_use", true);
    /* ------------------------------------------------------ */

    /* ---------------------Logging-------------------------- */
    public static SettingType<Boolean> DEBUG_TO_CHAT = new SettingType<Boolean>("debug_mode", false);
    public static SettingType<Boolean> LOG_TO_CONSOLE = new SettingType<Boolean>("console_logging", true);
    /* ------------------------------------------------------ */

    /* ---------------------Language Settings---------------- */
    public static SettingType<String> LANGUAGE_TO_SEND = new SettingType<String>("language_to_send", "english");
    public static SettingType<String> LANGUAGE_FROM_SEND = new SettingType<String>("language_from_send", "auto");
    public static SettingType<String> LANGUAGE_TO_RECEIVE = new SettingType<String>("language_to_receive", "english");
    public static SettingType<String> LANGUAGE_FROM_RECEIVE = new SettingType<String>("language_from_receive", "auto");
    /* ------------------------------------------------------ */

    /* ---------------------Event settings------------------- */
    public static SettingType<Boolean> SHOULD_TRANSLATE_SEND = new SettingType<Boolean>("should_translate_send", false);
    public static SettingType<Boolean> SHOULD_TRANSLATE_RECEIVE = new SettingType<Boolean>("should_translate_receive", false);
    public static SettingType<Boolean> SHOULD_TRANSLATE_ON_CLICK = new SettingType<Boolean>("should_translate_on_click", true);
    /* ------------------------------------------------------ */

    /* ---------------------Not used atm----------------------*/
    public static SettingType<Boolean> SHOULD_CACHE_TRANSLATIONS = new SettingType<Boolean>("should_cache_translations", false);
    public static SettingType<Boolean> WORD_BASED_CACHING = new SettingType<Boolean>("word_based_caching", false);
    public static SettingType<Boolean> USE_FALLBACK_API = new SettingType<Boolean>("use_fallback_api", false);
    public static SettingType<String> CHAT_SPLIT_REGEX = new SettingType<String>("chat_split_regex", "[0-9a-fk-or]");
    /* ------------------------------------------------------ */
}