package me.Indrit.translate.settings;

public class SettingType<T> {

    String key;
    T defValue;

    public SettingType(String key, T defVal) {
        this.key = key;
        this.defValue = defVal;
    }

    public String getKey() {
        return this.key;
    }

    public T getDefVal() {
        return this.defValue;
    }
}
